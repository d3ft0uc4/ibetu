var preloader = $("#preloader");
function check_channel_exists() {
    var wrong_channel = $("#wrong_channel");
    wrong_channel.hide();
    $.ajax({
        url: '/find_channel/',
        type: 'POST',
        data: {'channel': $("#channel_link").val()},
        success: function (response) {
            if (!JSON.parse(response)['result']) {
                wrong_channel.show();
            }
        }
    });
}
function check_key() {
    var error_message = $("#error_message");
    var error_value = $("#error_value");
    var forgot_key_btn = $("#forgot_key_btn");
    var btn = $("#part1button");
    error_message.hide();
    btn.hide();
    forgot_key_btn.show();
    $.ajax({
        url: '/get_challenge_stats/',
        type: 'POST',
        data: {'key': $("#key").val()},
        success: function (response) {
            var payload = JSON.parse(response);
            if (payload['winner']) {
                window.location.href = '/payout?id=' + $("#key").val();
            }
            else {
                error_value.html(payload['message']);
                error_message.show();
            }
        }
    });
}

function search_challenge() {
    $("#challenge_container").html('');
    $.ajax({
        url: '/search_challenge/',
        type: 'POST',
        data: {'search': $("#search").val()},
        success: function (response) {
            $("#challenge_container").html(response);
        }
    });
}

$("#channel_link").focusout(check_channel_exists);
$("#key").focusout(check_key);


$("#search").focusout(search_challenge);

function validate_form() {
    var email = $.trim($("#email").val());
    if (!email) {
        show_err("Введите email");
        return false;
    }
    if (!validateEmail(email)) {
        show_err("Некорректный email");
        return false;
    }
    var video_name = $("#video_name").val();
    if (!video_name) {
        show_err("Загрузите видео");
        return false;
    }
    var title = $("#name");
    if (title.length > 0 && !$.trim(title.val())) {
        show_err("Введите название");
        return false;
    }
    var description = $("#description");
    if (!$.trim(description.val())) {
        show_err("Введите описание");
        return false;
    }
    var date = $("#datepicker");
    if (date.length > 0 && !$.trim(date.val())) {
        show_err("Выберите дату окончания вызова");
        return false;
    }
    var price = $("#price");
    if (price.length > 0 && !$.trim(price.val())) {
        show_err("Введите сумму приза");
        return false;
    }
    if (price.length > 0 && parseInt(price.val()) <= 0) {
        show_err("Некорректная сумма приза");
        return false;
    }
    var search = $("#search");
    if (search.length > 0 && !$("input:radio[name ='challenge']:checked").val()) {
        show_err("Не выбран вызов");
        return false;
    }
    return true;
}

$("#part1button").click(function (e) {
    e.preventDefault();
    var form = $('#part1form')[0];
    var data = new FormData(form);

    $("#part1button").prop("disabled", true);
    if (!validate_form()) {
        $("#part1button").prop("disabled", false);
        return;
    }
    var url = $("#form_action").text();
    preloader.show();
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            preloader.hide();
            $("#part1button").prop("disabled", false);
            var d = JSON.parse(data);
            if (!d['success']) {
                show_err(d['message']);
            }
            else {
                var request_id = d['request_id'];
                if (d['need_payment']) {
                    window.location.href = '/pay?id=' + request_id;
                }
                else {
                    window.location.href = '/download_video?id=' + request_id;
                }

            }

        },
        error: function (data) {
            preloader.hide();
            $("#part1button").prop("disabled", false);
        }
    });
});

var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);

$('#datepicker').datepicker({
    minDate: tomorrow,
    dateFormat: 'yyyy-mm-dd',
    inline: true
});

$("#part2button").click(function (e) {
    window.location.href = '/';
});

$("#forgot_key_btn").click(function (e) {
    window.location.href = '/forgot_key';
});

$("#pay_btn").click(function (e) {
    var request_id = $(this).data('request');
    window.location.href = '/download_video?id=' + request_id;
});

$("#popup_close").click(function (e) {
    $("#popup").fadeOut();
});

function show_err(err) {
    $("#popup_message").html(err);
    $("#popup").fadeIn();
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}