# coding=utf-8
import json
from datetime import datetime
from django.http import HttpResponse
from ibetu.models import ChallengeAccepted, Challenge


def api_error(message):
    return to_json({'success': False, 'message': message})


def generate_description(_id):
    try:
        c = Challenge.objects.get(request_id=_id)
        return u'<br>'.join(
            [c.description,
             u'Код видео: {}'.format(c.request_id),
             u'Плейлист: https://www.youtube.com/playlist?list={}'.format(c.playlist_id),
             u'Срок окончания: {}'.format(datetime.strftime(c.date_end, '%d.%m.%Y')),
             u'Награда: {} руб.'.format(str(int(c.price)).decode('utf-8'))])
    except:
        c = ChallengeAccepted.objects.get(request_id=_id)
        return u'<br>'.join(
            [c.description,
             u'Код видео: {}'.format(c.request_id),
             u'Плейлист: https://www.youtube.com/playlist?list={}'.format(c.challenge.playlist_id),
             u'Срок окончания: {}'.format(datetime.strftime(c.challenge.date_end, '%d.%m.%Y')),
             u'Награда: {} руб.'.format(str(int(c.challenge.price)).decode('utf-8'))])


def generate_download_link(_id):
    try:
        c = Challenge.objects.get(request_id=_id)
    except:
        c = ChallengeAccepted.objects.get(request_id=_id)
    if c:
        return '{0}/{1}'.format(c.request_id, c.video_name)


def to_json(d):
    return HttpResponse(json.dumps(d))


def try_get_code(_id):
    """
    Если человек уже выложил видео,
    и оно было подхвачено ботом,
    не отдавать код по соображениям безопасности
    """
    try:
        ca = ChallengeAccepted.objects.get(request_id=_id)
        if not ca.video_url:
            return ca.token
        return ''
    except:
        return ''
