from django.core.management.base import BaseCommand
from ibetu.bot.api import add_to_playlist, find_video, find_views_count, find_channel_id, subscribe_to_user_channel
from ibetu.bot.builder import get_authenticated_service
from ibetu.bot.update_playlist import update_playlist
from ibetu.helpers import Map, send_congrats, send_winner_video
from ibetu.models import Challenge, ChallengeAccepted
import datetime
import shutil


class Command(BaseCommand):
    def handle(self, *args, **options):
        bind_main_videos()
        bind_accepted_videos()
        complete_challenges()


def bind_video(o, url, playlist_id):
    o.video_url = url
    video_id = url.split('=')[-1]
    channel_id = find_channel_id(Map({'video_id': video_id}))
    if channel_id:
        o.channel = 'https://www.youtube.com/channel/{}'.format(channel_id)
        subscribe_to_user_channel(Map({'channel_id': channel_id}))
        add_to_playlist(Map({'playlist_id': playlist_id, 'video_id': video_id}))
    delete_video_from_database(o.request_id)
    o.save()


def delete_video_from_database(request_id):
    directory = 'media/{0}'.format(request_id)
    shutil.rmtree(directory, ignore_errors=True)


def bind_main_videos():
    for o in Challenge.objects.filter(video_url='', finished=False, paid=True):
        url = find_video(o.request_id)
        if url:
            playlist_id = o.playlist_id
            bind_video(o, url, playlist_id)
            youtube = get_authenticated_service()
            update_playlist(youtube,
                            Map({'playlist_id': playlist_id,
                                 'title': o.title,
                                 'privacyStatus': 'public'}))
            o.save()


def bind_accepted_videos():
    for o in ChallengeAccepted.objects.filter(challenge__finished=False).exclude(challenge__video_url=''):
        if o.video_url == '':
            url = find_video(o.request_id)
            if url:
                bind_video(o, url, o.challenge.playlist_id)


def update_video_stats():
    accepted = ChallengeAccepted.objects.filter(challenge__finished=False).exclude(video_url='')
    for a in accepted:
        video_id = a.video_url.split('=')[-1]
        try:
            views = find_views_count(Map({'video_id': video_id}))
            a.views_count = views
        except:
            a.video_url = ''
        a.save()


def complete_challenges():
    update_video_stats()
    for c in Challenge.objects.filter(finished=False, paid=True):
        if c.date_end < datetime.datetime.date(datetime.datetime.now()):
            accepted = ChallengeAccepted.objects.filter(challenge=c).exclude(video_url='')
            if accepted.count():
                token = accepted.latest('views_count').token
                c.winner_token = token
                send_congrats(token)
                send_winner_video(token)
            c.finished = True
            c.save()
