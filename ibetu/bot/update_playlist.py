def update_playlist(youtube, args):
    request = youtube.playlists().update(
        part="snippet,status",
        body={
            "id": args.playlist_id,
            "status": {
                "privacyStatus": args.privacyStatus
            },
            "snippet": {
                "title": args.title,
                "description": args.description
            }
        }
    )
    response = request.execute()
