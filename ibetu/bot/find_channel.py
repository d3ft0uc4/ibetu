#!/usr/bin/python

def check_channel_exists(youtube, _id):
    results = youtube.channels().list(
        part='id',
        id=_id
    ).execute()
    if results['items']:
        return True
    return False
