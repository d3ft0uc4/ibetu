#!/usr/bin/python

import argparse
from googleapiclient.errors import HttpError

from ibetu.bot.builder import get_authenticated_service


def add_subscription(youtube, args):
    add_subscription_response = youtube.subscriptions().insert(
        part='snippet',
        body=dict(
            snippet=dict(
                resourceId=dict(
                    kind='youtube#channel',
                    channelId=args.channel_id
                )
            )
        )).execute()

    return add_subscription_response['snippet']['title']


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('--channel_id', help='ID of the channel to subscribe to.',
                        default='UC_x5XG1OV2P6uZZ5FSM9Ttw')
    args = parser.parse_args()

    youtube = get_authenticated_service()
    try:
        channel_title = add_subscription(youtube, args)
    except HttpError, e:
        print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
