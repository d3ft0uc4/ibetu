#!/usr/bin/python


def get_free_playlist(youtube):
    request = youtube.playlists().list(
        part='snippet',
        maxResults=50,
        mine=True
    )
    response = request.execute()
    for i in response['items']:
        if i['snippet']['title'] == 'IBETU-SYSTEM':
            return i['id']
