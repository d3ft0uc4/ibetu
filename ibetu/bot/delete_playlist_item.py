#!/usr/bin/python


def delete_playlist_item(youtube, args):
    request = youtube.playlistItems().delete(
        id=args.id
    )
    request.execute()
