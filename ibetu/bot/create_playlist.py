#!/usr/bin/python


import argparse
from googleapiclient.errors import HttpError

from ibetu.bot.builder import get_authenticated_service


def add_playlist(youtube, args):
    body = dict(
        snippet=dict(
            title=args.title,
            description=args.description
        ),
        status=dict(
            privacyStatus='public'
        )
    )

    playlists_insert_response = youtube.playlists().insert(
        part='snippet,status',
        body=body
    ).execute()

    print 'New playlist ID: %s' % playlists_insert_response['id']
    return playlists_insert_response['id']


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--title',
                        default='Test Playlist',
                        help='The title of the new playlist.')
    parser.add_argument('--description',
                        default='A private playlist created with the YouTube Data API.',
                        help='The description of the new playlist.')

    args = parser.parse_args()

    youtube = get_authenticated_service()
    try:
        add_playlist(youtube, args)
    except HttpError, e:
        print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
