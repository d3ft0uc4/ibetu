#!/usr/bin/python
import argparse
from googleapiclient.errors import HttpError
from ibetu.bot.builder import get_authenticated_service


def find_channel(youtube, args):
    request = youtube.videos().list(
        part="snippet",
        id=args.video_id
    )
    response = request.execute()
    if response['items']:
        return response['items'][0]['snippet']['channelId']
    return ''


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--video_id',
                        required=True)

    args = parser.parse_args()

    youtube = get_authenticated_service()
    try:
        find_channel(youtube, args)
    except HttpError, e:
        print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
