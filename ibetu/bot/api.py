import re
import requests
import json
from bs4 import BeautifulSoup
# from googlesearch import search_videos
from ibetu.bot.delete_playlist_item import delete_playlist_item
from ibetu.bot.find_channel import check_channel_exists
from ibetu.bot.add_to_playlist import add_playlist_item
from ibetu.bot.builder import get_authenticated_service, get_google_search_service
from ibetu.bot.create_playlist import add_playlist
from ibetu.bot.find_playlist import get_free_playlist
from ibetu.bot.find_playlist_items import get_playlist_item
from ibetu.bot.find_video_channel import find_channel
from ibetu.bot.find_video_stats import find_views
from ibetu.bot.subscribe_to_channel import add_subscription
from ibetu.bot.update_playlist import update_playlist
from ibetu.bot.upload import upload_video
from ibetu.helpers import Map
from ibetu.proxy_rotation import proxy_pool


def upload(options):
    youtube = get_authenticated_service()
    return upload_video(youtube, options)


def create_playlist(options):
    youtube = get_authenticated_service()
    playlist_id = get_free_playlist(youtube)
    playlist_item_id = get_playlist_item(youtube, Map({'playlist_id': playlist_id}))
    delete_playlist_item(youtube, Map({'id': playlist_item_id}))
    update_playlist(youtube,
                    Map({'playlist_id': playlist_id,
                         'title': options.title,
                         'description': options.description,
                         'privacyStatus': 'private'}))
    return playlist_id


# def create_playlist(options):
#     youtube = get_authenticated_service()
#     return add_playlist(youtube, options)
#


def channel_exists(url):
    youtube = get_authenticated_service()
    _id = url.split('/')[-1]
    return check_channel_exists(youtube, _id)


def search_videos(q):
    for method in [search_direct, search_proxy, search_google, search_bing]:
        try:
            result = method(q)
            if result[0]:
                print('Found by method:{}'.format(method.__name__))
                return result
        except:
            print('Skipping method:{}'.format(method.__name__))
    return ['']


def search_google(q):
    service = get_google_search_service()
    res = service.cse().list(
        q=q,
        cx='002490211646375861432:bbsj_hre0uw',
    ).execute()
    items = res.get('items')
    if not items:
        return ['']
    return [items[0]['formattedUrl']]


def search_bing(q):
    headers = {
        'Accept': 'application/json',
        'Ocp-Apim-Subscription-Key': '713c655ea6c846269e9ba2c1cdad3742',
        'X-MSEdge-ClientID': '0',
        'count': '1'
    }

    payload = {
        # 'cc': 'us',  # Exclusive with mkt.
        'q': q,
        'mkt': 'en-US',
        'responseFormat': 'JSON',
        'safeSearch': 'Moderate',
        'setLang': 'en-US',
    }

    r = requests.get('https://api.cognitive.microsoft.com/bing/v7.0/search', params=payload, headers=headers)
    if r.status_code == 200:
        response = r.json()
        if response.get('webPages'):
            v = response['webPages'].get('value', [{}])[0].get('url', '')
            return [v]
    return ['']


def search_direct(q):
    html_content = requests.get("http://www.youtube.com/results?search_query=" + q).text
    search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content)
    return ["http://www.youtube.com/watch?v=" + search_results[0]]


def search_proxy(q):
    for i in range(1, 5):
        proxy = next(proxy_pool)
        try:
            html_content = requests.get("http://www.youtube.com/results?search_query=" + q,
                                        proxies={"http": proxy, "https": proxy}, timeout=10).text
            search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content)
            return ["http://www.youtube.com/watch?v=" + search_results[0]]
        except:
            print('Skipping proxy:{}'.format(proxy))


def find_video(code):
    q = '"{0}"'.format(code)
    for url in search_videos(q):
        if 'watch' in url:
            return url
        if 'channel' in url:
            soup = BeautifulSoup(requests.get(url).content)
            pattern = re.compile(str(code))
            columns = soup.findAll('div', text=pattern)
            if columns:
                return 'https://youtube.com' + columns[0].parent.parent.find('h3').find('a', href=True)['href']


def add_to_playlist(options):
    youtube = get_authenticated_service()
    return add_playlist_item(youtube, options)


def subscribe_to_user_channel(options):
    youtube = get_authenticated_service()
    return add_subscription(youtube, options)


def invite_to_channel(options):
    youtube = get_authenticated_service()
    return None


def find_views_count(options):
    youtube = get_authenticated_service()
    return find_views(youtube, options)


def find_channel_id(options):
    youtube = get_authenticated_service()
    return find_channel(youtube, options)
