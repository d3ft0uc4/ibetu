#!/usr/bin/python


import argparse
from googleapiclient.errors import HttpError
from ibetu.bot.builder import get_authenticated_service


def add_playlist_item(youtube, args):
    response = youtube.playlistItems().insert(
        part='snippet',
        body=dict(
            snippet=dict(
                playlistId=args.playlist_id,
                resourceId=dict(
                    kind="youtube#video",
                    videoId=args.video_id
                )
            )
        )
    ).execute()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--playlist_id',
                        required=True)

    parser.add_argument('--video_id',
                        required=True)

    args = parser.parse_args()

    youtube = get_authenticated_service()
    try:
        add_playlist_item(youtube, args)
    except HttpError, e:
        print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)
