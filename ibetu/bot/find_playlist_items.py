#!/usr/bin/python


def get_playlist_item(youtube, args):
    request = youtube.playlistItems().list(
        part='id',
        playlistId=args.playlist_id
    )
    response = request.execute()
    for r in response['items']:
        return r['id']
