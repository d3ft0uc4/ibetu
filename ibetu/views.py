# coding=utf-8
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from bot import api
from ibetu.helpers import Map, send_subscribe, validate_uuid4, send_forgot_keys
import uuid
import json
from ibetu.models import Challenge, ChallengeAccepted
from datetime import datetime
from ibetu.utils import to_json, generate_description, try_get_code, api_error, generate_download_link
import traceback
from django.core.files.storage import FileSystemStorage
from django.views.static import serve
import os
import mimetypes
import subprocess


class IndexView(View):
    def get(self, request):
        return render(request, 'index.html')


class AcceptChallengeView(View):
    def get(self, request):
        return render(request, 'accept_challenge.html',
                      {'challenges': Challenge.objects.filter(finished=False, paid=True)})


class ClearSafeView(View):
    def get(self, request):
        return render(request, 'clear_safe.html')


def forgot_key(request):
    mail = request.GET.get('email')
    if mail:
        send_forgot_keys(mail)
        return render(request, 'forgot_key_success.html')
    return render(request, 'forgot_key.html')


@csrf_exempt
def create_challenge(request):
    try:
        form = request.POST
        video = request.FILES.get('video')
        request_id = str(uuid.uuid4())
        c = Challenge()
        c.request_id = request_id
        # c.channel = form.get('channel')
        # if not api.channel_exists(c.channel):
        #     return api_error('Канал не найден')
        c.email = form.get('email')
        c.title = form.get('name')
        if not form.get('date'):
            return api_error('Дата не может быть меньше текущей')
        c.date_end = datetime.strptime(form.get('date'), '%Y-%m-%d')
        if c.date_end < datetime.now():
            return api_error('Дата не может быть меньше текущей')
        c.description = form.get('description')
        if not form.get('price') or float(form.get('price')) <= 0.0:
            return api_error('Некорректная сумма')
        c.price = float(form.get('price'))
        if not video:
            return api_error('Загрузите видео')
        # c.video = video.read()
        fs = FileSystemStorage(location='media/{0}/'.format(request_id))
        video_name = 'video'
        filename = fs.save(video_name, video)
        uid = str(uuid.uuid4())
        p = 'media/{0}/{1}'.format(request_id, video_name)
        o = 'media/{0}/{1}.mp4'.format(request_id, uid)
        command = \
            "ffmpeg -i '{0}' -i watermark.png -filter_complex \"[0:v][1:v]overlay\" -codec:a copy -f mp4 '{1}'" \
                .format(p, o)
        subprocess.call(command, shell=True)
        uploaded_file_url = fs.url(filename)
        c.video_path = uploaded_file_url
        c.video_name = uid + '.mp4'
        c.finished = False
        # TODO Remove after payment gate is connected
        c.paid = True
        playlist_id = api.create_playlist(Map({'title': form.get('name'),
                                               'description': form.get('description')}))
        if not playlist_id:
            raise Exception('Youtube technical error - playlist not found')
        c.playlist_id = playlist_id
        c.save()
        send_subscribe(c.email)
        return HttpResponse(to_json({'success': True, 'need_payment': True, 'request_id': request_id}))
    except Exception as e:
        return api_error(traceback.format_exc())


def pay(request):
    _id = request.GET.get("id")
    objs = Challenge.objects.filter(request_id=_id)
    if not objs.count():
        raise Http404
    obj = objs[0]
    return render(request, 'payment.html',
                  {'title': 'Выберите способ оплаты',
                   'btn': 'pay_btn',
                   'price': obj.price,
                   'request_id': _id,
                   'btn_label': 'Оплатить'
                   }
                  )


def payout(request):
    _id = request.GET.get("id")
    objs = ChallengeAccepted.objects.filter(token=_id)
    if not objs.count():
        raise Http404
    obj = objs[0]
    return render(request, 'payment.html',
                  {'title': 'Выберите способ выплаты:',
                   'price': obj.challenge.price,
                   'btn': 'part2button',
                   'request_id': _id,
                   'btn_label': 'Вывести средства'
                   }
                  )


@csrf_exempt
def create_accept_challenge(request):
    try:
        form = request.POST
        request_id = str(uuid.uuid4())
        video = request.FILES.get('video')
        c = ChallengeAccepted()
        # c.channel = form.get('channel')
        # if not api.channel_exists(c.channel):
        #     return api_error('Канал не найден')
        c.email = form.get('email')
        c.request_id = request_id
        c.description = form.get('description')
        if not form.get('challenge'):
            return api_error('Не выбран вызов')
        c.challenge = Challenge.objects.get(request_id=form.get('challenge'))
        if not video:
            return api_error('Загрузите видео')
        # c.video = video.read()
        fs = FileSystemStorage(location='media/{0}/'.format(request_id))
        video_name = 'video'
        filename = fs.save(video_name, video)
        uploaded_file_url = fs.url(filename)
        uid = str(uuid.uuid4())
        p = 'media/{0}/{1}'.format(request_id, video_name)
        o = 'media/{0}/{1}.mp4'.format(request_id, uid)
        command = \
            "ffmpeg -i '{0}' -i watermark.png -filter_complex \"[0:v][1:v]overlay\" -codec:a copy -f mp4 '{1}'" \
                .format(p, o)
        subprocess.call(command, shell=True)
        c.video_path = uploaded_file_url
        c.video_name = uid + '.mp4'
        c.token = str(uuid.uuid4())
        c.finished = False
        c.views_count = 0
        c.save()
        send_subscribe(c.email)
        return to_json({'success': True, 'request_id': request_id})
    except Exception as e:
        return api_error(traceback.format_exc())


def download_video(request):
    _id = request.GET.get('id')
    description = generate_description(_id)
    link = generate_download_link(_id)
    return render(request, 'download_video.html',
                  {'description': description,
                   'id': _id,
                   'link': link,
                   'code': try_get_code(_id)})


def serve_video(request, url):
    for filepath in _get_filepaths(url):
        if os.path.exists(filepath):
            with open(filepath, 'rb') as fh:
                content_type = mimetypes.guess_type(os.path.basename(filepath))[0]
                if not content_type:
                    content_type = 'application/octet-stream'
                response = HttpResponse(fh.read(), content_type=content_type)
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(filepath)
                return response
    raise Http404


def _get_filepaths(url):
    return ['media/{0}'.format(url)]


@csrf_exempt
def find_channel(request):
    link = request.POST.get('channel')
    result = api.channel_exists(link)
    return HttpResponse(json.dumps(dict(result=result)))


@csrf_exempt
def get_challenge_stats(request):
    token = request.POST.get('key').strip()
    objs = ChallengeAccepted.objects.filter(token=token)
    if not objs.count():
        return api_error('Ключ не найден')
    obj = objs[0]
    if not obj.video_url:
        return api_error('Видео обрабатывается нашим роботом')
    challenge = obj.challenge
    if challenge.finished:
        if obj.token == challenge.winner_token:
            return to_json({'success': True,
                            'message': 'Поздравляем, Вы выиграли!\nСумма выигрыша:{} руб.'.format(challenge.price),
                            'winner': True})
    competitors = ChallengeAccepted.objects.filter(challenge=challenge).exclude(video_url='').order_by('-views_count')
    place = 0
    for c in competitors:
        place += 1
        if c.token == token:
            break

    msg = u'<br>'.join([
        u'Срок окончания: {}'.format(datetime.strftime(challenge.date_end, '%d.%m.%Y')),
        u'Место в рейтинге: {}/{}'.format(str(int(place)).decode('utf-8'), str(competitors.count()).decode('utf-8'))
    ])
    return to_json({'success': True, 'message': msg, 'winner': False})


@csrf_exempt
def search_challenge(request):
    search = request.POST.get('search').strip()
    objs = None
    if 'playlist' in search:
        objs = Challenge.objects.filter(playlist_id=search.split('=')[-1], finished=False, paid=True)
    elif 'channel' in search:
        objs = Challenge.objects.filter(channel=search, finished=False, paid=True)
    elif validate_uuid4(search):
        objs = Challenge.objects.filter(request_id=search, finished=False, paid=True)
        if not objs.count():
            objs = Challenge.objects.filter(challengeaccepted__request_id=search, finished=False, paid=True)
    if objs and objs.count():
        return render(request, 'helpers/challenge_radio.html',
                      {'challenges': objs})
    return render(request, 'helpers/nothing_found.html')
