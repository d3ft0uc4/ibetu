# coding=utf-8
from django.db import models
from datetime import datetime


class Challenge(models.Model):
    request_id = models.CharField(max_length=4000)
    channel = models.CharField(max_length=4000)
    video = models.BinaryField()
    video_name = models.CharField(max_length=4000)
    title = models.CharField(max_length=4000)
    description = models.CharField(max_length=4000)
    date_end = models.DateField(default=datetime.now())
    price = models.FloatField(default=0.0)
    finished = models.BooleanField()
    video_url = models.CharField(max_length=4000)
    paid = models.BooleanField(default=False)
    playlist_id = models.CharField(max_length=4000)
    winner_token = models.CharField(max_length=4000)
    video_path = models.CharField(max_length=4000)
    email = models.CharField(max_length=4000)
    retry_count = models.IntegerField(default=0)


class ChallengeAccepted(models.Model):
    request_id = models.CharField(max_length=4000)
    channel = models.CharField(max_length=4000)
    video = models.BinaryField()
    video_name = models.CharField(max_length=4000)
    description = models.CharField(max_length=4000)
    challenge = models.ForeignKey(Challenge)
    finished = models.BooleanField(default=False)
    video_url = models.CharField(max_length=4000)
    playlist_id = models.CharField(max_length=4000)
    token = models.CharField(max_length=4000)
    views_count = models.IntegerField()
    video_path = models.CharField(max_length=4000)
    email = models.CharField(max_length=4000)
    retry_count = models.IntegerField(default=0)


class UserManager(models.Manager):
    def validator(self, post_data):
        errors = {}
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=4000)
    last_name = models.CharField(max_length=4000)
    channel = models.CharField(max_length=4000)
    password = models.CharField(max_length=4000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    email = models.CharField(max_length=4000)
    objects = UserManager()
