# coding=utf-8
from ibetu import sendmail
from ibetu.models import ChallengeAccepted
from uuid import UUID

from ibetu.settings import HOSTNAME


class Map(dict):
    """
    Example:
    m = Map({'first_name': 'Eduardo'}, last_name='Pool', age=24, sports=['Soccer'])
    """

    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.iteritems():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.iteritems():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]


def validate_uuid4(uuid_string):
    try:
        val = UUID(uuid_string, version=4)
    except ValueError:
        return False

    return True


def send_subscribe(to):
    body = u'Не забудьте подписаться на канал:\n{}'.format(
        u'https://www.youtube.com/channel/UChnkjhxYh3hqUzhug0D8IHA?sub_confirmation=1')
    return sendmail.send(to, u'Приветствуем на сервисе IBetU', body)


def send_forgot_keys(to):
    objs = ChallengeAccepted.objects.filter(email=to)
    if objs.count():
        body = u'Ваши ключи:\n'
        for o in objs:
            body += u'Вызов: {} - Ключ: {}'.format(o.challenge.title, o.token)
            body += u'\n'
        return sendmail.send(to, u'Восстановление ключей', body)


def send_congrats(token):
    winner = ChallengeAccepted.objects.get(token=token)
    body = u'Поздравляем, Вы выиграли в вызове!\nВывести средства можно по ссылке:\n' \
           u'http://{}/payout?id={}' \
        .format(HOSTNAME, token)
    return sendmail.send(winner.email, u'Вы выиграли в вызове!', body)


def send_winner_video(token):
    winner = ChallengeAccepted.objects.get(token=token)
    video_url = winner.video_url
    challenge = winner.challenge
    emails = []
    for a in ChallengeAccepted.objects.filter(challenge=challenge).exclude(token=token):
        emails.append(a.email)
    emails.append(challenge.email)
    body = u'В вызове - {} победило видео:\n{}'.format(challenge.title, video_url)
    for email in set(emails):
        return sendmail.send(email, u'Результаты вызова', body)
