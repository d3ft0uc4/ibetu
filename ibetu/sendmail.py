import smtplib
from ibetu.settings import GMAIL_USER, GMAIL_PASSWORD
from email.mime.text import MIMEText
from email.header import Header

gmail_user = GMAIL_USER
gmail_password = GMAIL_PASSWORD

sent_from = 'IBetU'


def send(to, subject, body):
    msg = MIMEText(body, 'plain', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')
    msg['From'] = sent_from
    msg['To'] = to
    # try:
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.sendmail(msg['From'], to, msg.as_string())
    server.close()
    return 'ok'
    # except Exception as e:
    #     return e.message

