"""ibetu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from ibetu.views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^create_challenge', create_challenge, name='create_challenge'),
    url(r'^accept_challenge/$', AcceptChallengeView.as_view(), name='accept_challenge'),
    url(r'clear_safe', ClearSafeView.as_view(), name='clear_safe'),
    url(r'^create_accept_challenge', create_accept_challenge, name='create_accept_challenge'),
    url(r'find_channel', find_channel),
    url(r'download_video', download_video),
    url(r'forgot_key', forgot_key, name='forgot_key'),
    url(r'serve_video/(?P<url>.*)/$', serve_video),
    url(r'get_challenge_stats', get_challenge_stats),
    url(r'search_challenge', search_challenge),
    url(r'payout', payout),
    url(r'pay', pay)

]
