import time
from selenium import webdriver

from ibetu.settings import GMAIL_USER, GMAIL_PASSWORD

login_url = 'https://accounts.google.com/ServiceLogin?passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Fapp%3Ddesktop%26next%3D%252F%26hl%3Dru%26action_handle_signin%3Dtrue&hl=ru&uilel=3&service=youtube'
email = GMAIL_USER
password = GMAIL_PASSWORD


def main():
    browser = webdriver.Chrome()
    try:
        browser.get(login_url)
        elem = browser.find_element_by_id('identifierId')
        elem.send_keys(email)
        browser.find_element_by_id('identifierNext').click()
        time.sleep(1.5)
        browser.find_element_by_xpath('//*[@id="password"]/div[1]/div/div[1]/input').send_keys(password)
        browser.find_element_by_id('passwordNext').click()
        browser.get('https://www.youtube.com/watch?v=xhStLuqskSc')
        time.sleep(5)
        for i in xrange(100):
            time.sleep(1.5)
            browser.find_element_by_xpath('//*[@id="top-level-buttons"]/ytd-button-renderer[2]').click()
            time.sleep(1.5)
            browser.find_element_by_xpath('//*[@id="endpoint"]').click()
            browser.find_element_by_xpath('//*[@id="input-1"]/input').send_keys('IBETU-SYSTEM')
            browser.find_element_by_id('input-2').click()
            time.sleep(0.5)
            browser.find_element_by_xpath('//*[@id="entries"]/ytd-privacy-dropdown-item-renderer[3]').click()
            time.sleep(0.5)
            browser.find_element_by_xpath('//*[@id="actions"]/ytd-button-renderer').click()
            time.sleep(5)
    except:
        pass
    finally:
        browser.quit()


if __name__ == '__main__':
    while True:
        main()
